---
marp: false
title: 마이크로서비스와 깃랩
description: 
// theme: uncover
theme: clean
paginate: true
_paginate: true
page_number: true
footer: Microservice & GitLab
---

<style scoped>li {font-size:80%;} </style>

# <!--fit-->  마이크로서비스와 GitLab

> 개발자를 위한! 
> 마이크로서비스, 빠르게 시작하는 방법

<style scoped>h6 { text-shadow: 0 0 20px #fff; text-align: center; }</style>

###### 신철호

---

<style scoped>li,h2 {font-size:70%;} </style>

# GitLab Korea 커뮤니티 소개
## 오프라인
- 2018년 커뮤니티 오픈 - @gyoo님 시작
- 2019년 3월 - 1번째 GitLab 오프라인 모임
- 2019년 9월 - 5번째 GitLab 오프라인 모임  `9/25(수) 19:00 @마루180`

## 온라인
- **소통**
  - Facebook으로 일반 정보공유
  - Slack으로 뉴스/기술관련 소통 
- **협업** : gitlab.com/gitlab-kr 그룹에서
  - Meetup, Workshop 자료 공유
  - 관련 프로젝트, 샘플 프로젝트 협업
  - 주요 백서, 문서, 블로그 번역 협업
---

<!--
_backgroundColor: #123
_color: #fff
-->


# 👉 발표내용

 > 마이크로서비스 트랜드
 > 마이크로서비스 리뷰
 > 마이크로서비스 구축사례
 > 마이크로서비스 아키텍처 구축 데모

---
# 발표자 소개 :smirk:

## 프로페셔널
- 2007 ~ 전문연구요원으로 웹/모바일 R&D/SW 다양한 개발 경험
- 2013 ~ 프로젝트리서치 설립 (co-founder)
  - 솔루션 서드파티/가시화도구 개발, Agile, ALM 컨설팅
  - DevOps, CI/CD, 릴리즈관리 솔루션 아키텍트
## 아마추어
- 목수/스노우보딩/웨이크보딩/스쿠버다이빙
- 후추통 100개/하프파이프 반키/투웨이크 쓰리/코모도

---

<!--
_backgroundColor: #123
_color: #fff
-->
# <!--fit-->  :relaxed: 마이크로서비스 트랜드 :heart_eyes:   

---

![bg right 90%](images/msa-trend-dzone.png)

### 엔터프라이즈 회사들의 주류 아키텍처
- `91%` 사용중이거나 사용 계획
- `86%` 향후 5년내 기본 아키텍처 예정
- `92%` 전년 대비 서비스 수가 증가함

---

![bg right 90%](images/why-use-dzone.png)

### 주요가치 => 확장성, Agility 
- `69%` 서비스 쉬운확장, 어떤 부분은 더 빠르게 확장
- `35%` 장애원인을 특정 앱으로
- `34%` 아키텍처 실험

---

![bg right 90%](images/challenges-building-dzone.png)

### 어려운점 => 복잡함
- `70%` 컨테이너 사용
- `27%` 개발에만 컨테이너 사용
- `35%` 개발과 프로덕션 모두 컨테이너 사용
- `51%` 쿠버네티스로 오케스트레이션

---

<!--
_backgroundColor: #123
_color: #fff
-->

# <!--fit-->  왜 마이크로서비스인가?


---
## 모놀리틱 아키텍처 - Uber App

![right bg 100%](images/Richardson-microservices-part1-1_monolithic-architecture.png)

###### 장점
- 단순, 심플함 = MVP를 빠르게 생산
###### 단점
- 코드베이스가 커질수록 문제 발생
  - 생산성X, 확장성X, 배포자유:sleepy:
- SPOF(Single Point of Failure)
- 신기술 X :rage:

---
# ...결국 비즈니스 가치를 기민하게 제공하기 어려움.

![h:350](images/org-flow.png)

---

## 마이크로서비스 아키텍처 정의
> 어플리케이션을 독립적으로 배치 가능한 서비스 조합으로 설계하는 방식. 
> @마틴파울러

> 어플리케이션을 느슨하게 결합된 서비스들의 모음으로 구조화 하는 SOA의 구조화 방식 
> @위키피디아

![bg right 100%](images/msa-definition.png)

---

## 마이크로서비스 아키텍처 

![left bg 80%](images/Richardson-microservices-part1-2_microservices-architecture.png)

<style scoped>li {font-size:80%;}</style>

###### 장점
- 작은 서비스, 빠르고 쉽게 개발
- 독립적인 배포, 독립적인 배포
- 폴리글랏
###### 단점
- 서비스간 트래픽이슈, 트랜잭션 이슈
- 모니터링, 로깅, 분산데이터 운영/관리의 복잡함
- 통합 테스트가 어려움

---
<style scoped>h6 { text-shadow: 0 0 20px #fff; text-align: center; } img { align:center;}</style>

![fit center](images/Richardson-microservices-part1-3_scale-cube.png)

###### X : 서비스 복제 , Y : 서비스 분리,  Z : 데이터 분리저장 =>  즉,Y 분리해서 X 확장 하는것

---

## 마이크로서비스를 한다는 것은..
- 단지 기술적 아키텍쳐가 아닌,
- 비즈니스 역량에 따른 조직 문제를 해결 하기위한 솔루션! (Conway's Law)
- 프로젝트가 아니라 프로덕트

![h:400](images/MSA-Silio.png)

---

## 아마존의 쇼핑몰 모바일 앱

![h:400](images/Richardson-microservices-part2-1_amazon-apps.png)

> 상품정보, 쇼핑카트, 주문내역, 고객리뷰, 재고수, 배송옵션, 추천상품, 구매 옵션...

---

## 마이크로서비스 클라이언트 

하나의 정보를 보여주기 위해 
수많은 서비스를 직접 호출?

> 한번에 가져오기 vs 나눈서비스로 가져오기 


![bg right 90%](images/Richardson-microservices-part2-2_microservices-client.png)

---

## API 게이트웨이가 필요

- 서비스 게이트웨이를 통해서 호출
- 요청에 따라 필요 서비스 라우팅
- 필요한 서비스만 노출하여 캡슐화
- 클라이언트, 버젼별 API 제공
> **보틀넥과, 게이트웨이 장애시 문제해결 방안필요!**

![bg right 90%](images/Richardson-microservices-part2-3_api-gateway.png)

---

## 클라우드에서 마이크로서비스하려고 보니,

어디 경로/포트로 요청해야하지? 
> 필요한 서비스를 찾는 방법? = 서비스 디스커버리

![bg right 90%](images/Richardson-microservices-part4-1_difficult-service-discovery.png)

---

### 클라이언트쪽 서비스 디스커버리 방식
1. 서비스 시작시 레지스트리 등록
2. 레지스트리는 서비스 상태 체크
3. 클라이언트는 서비스 중 하나를 골라 요청 (로드밸런싱)

![bg right 90%](images/Richardson-microservices-part4-2_client-side-pattern.png)

---

### 서버쪽 서비스 디스커버리 방식
1. 서비스 시작시 레지스트리에 등록
2. 클라이언트는 로드밸런서에 가용 서비스 요청
3. 로드밸런서는 레지스트리에 등록된 서비스 선택 

![bg left 110%](images/Richardson-microservices-part4-3_server-side-pattern.png)

---

## API 게이트웨이 역할의 중심에는 서비스 레지스트리가 있다.
- 서비스 인스턴스의 접속 위치 정보를 저장하는 DB
- 셀프 등록 패턴 : 서비스 스스로 등록을 관리
- 서드파티 등록 패턴 : 서드파티가 등록을 관리

---

<!--
_backgroundColor: #123
_color: #fff
-->

# <!--fit-->  마이크로서비스 아키텍처 시작사례 

---

## 모놀리스에서 마이크로서비스로 리펙토링
> 폭파시키고 처음부터 작성해야 할 때는 진짜로 폭파했을때 뿐이다. 
> @마틴파울러

![bg right 100%](images/Richardson-microservices-part7-fig.png)

---

## 어느 고객사의 여정 

고객님 왈: 
> Oracle Siebel CRM! 년 수십억씩 쓰는건 낭비야!
**`마이크로서비스로 전환해야 살아남아!`** 
**`방안들고와~!`** :pouting_cat:

![bg right 100%](images/ESB.png)

---

## 교육 + 조직구성
- 변할 수 없는 사일로?
  > feat. [Five monkeys experiment](http://ko.experiments.wikidok.net/wp-d/59e55535696a9c5c3748a3e2/View)
- 변화 가능한 사일로?
  > feat. 교육 - Agile, DevOps, 인프라, 기술스택, 솔루션

![bg right 100%](images/MSA-Enables.png)

--- 

<style scoped>li {font-size:80%;}</style>

# <!-- fit --> 스트랭글러 패턴
## 1단계 : 매크로 서비스
- ESB를 쓰는 SOAP 서비스들을 Restful하게
- Swagger로 Restful API 문서화
- 기본 데이터 모델을 준비

## 2단계 : 미니서비스
- 서비스 분할을 시작 
- 하나의 서비스를 하나의 컨테이너에

![bg right 100%](images/Richardson-microservices-part7-pull-module-from-monolith.png)

---

<style scoped>li {font-size:80%;}</style>

## 3단계 : 마이크로서비스 
- 마이크로서비스 배포 파이프라인을 구축
- 독립 배포 가능 파이프라인 

## 4단계 : 엔터프라이즈 마이크로서비스
- 서비스디스커버리, 
  `API 게이트웨이 고도화`
- 완전히 확장 가능함
  `마이크로서비스 에코시스템 구축`

![bg right 100%](images/Richardson-microservices-part7-refactoring.png)

--- 


## 마이크로서비스 에코시스템 설계

![h:500](images/msa-sample-architecture.png)

---

## 전체 워크플로우

![h:500](images/msa-sample-workflow.png)

---

## 워크플로우 - 개발업무처리 칸반

![h:500](images/msa-sample-kanban.png)

---

## 워크플로우 - Git

![h:500](images/msa-sample-git-workflow.png)

---

<!--
_backgroundColor: #123
_color: #fff
-->

# <!--fit-->  마이크로서비스 구축하기 데모
## <!--fit--> [ JHipster + Docker + GitLab => Rocks! :metal: ]

---

## 데모 아키텍처

![h:400](images/jhipster-msa.png)

<style scoped>li { font-size:60%; }</style>

- `Service Discovery` : JHipster Register, Eureka, Spring cloud config server
- `API Gateway` : Netflix Zuul, ReactJS, Swagger
- `Microservice App`: Spring Boot
- `Monitoring` : JHipster Console, ELK, Zipkin

---

<!--
_backgroundColor: #000
_color: #fff
-->

# <!-- fit --> [Demo](demo.md)

---

#  Q&A

---


## Referneces 
- [2018 DZone Guide to Microservices @DZone](https://apifriends.com/api-management/dzone-guide/)
- [Designing and Deploying Microservices @NGINX](https://www.nginx.com/resources/library/designing-deploying-microservices/)
- [왜 우리는 마이크로서비스를 구현하고자 하는가?](https://www.slideshare.net/awskorea/application-of-ci-cd-for-micro-service-architecture-kim-min-sung?from_action=save)
- [Create full Microservice stack using JHipster](https://medium.com/jhipster/create-full-microservice-stack-using-jhipster-domain-language-under-30-minutes-ecc6e7fc3f77)
