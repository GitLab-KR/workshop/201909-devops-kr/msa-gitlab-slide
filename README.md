
# MSA와 GitLab 

[2019년 9월 19일 DevOps Korea 야간과외](https://meetup.devopskorea.org/201909)에서 발표된 슬라이드 입니다.

# 슬라이드 보기
슬라이드는 [marp-cli](https://github.com/marp-team/marp-cli)를 이용해서 Markdown 으로 작성되었습니다.

## GitLab Page 에서 보기
브라우져로 다음 페이지를 열면 슬라이드가 열립니다.
- https://gitlab-kr.gitlab.io/workshop/201909-devops-kr/msa-gitlab-slide

## Docker 실행해서 보기
```
$ docker run --name 201909-devops-kr-slide -p 9090:80 registry.gitlab.com/gitlab-kr/workshop/201909-devops-kr/msa-gitlab-slide:master 
```
브라우져를 통해 **http://localhost:9090** 에서 확인 할 수 있습니다.


## 체크아웃 받아서 보기
node 8.x 이상이 필요 합니다.

```
$ git clone https://gitlab.com/GitLab-KR/workshop/201909-devops-kr/msa-gitlab-slide.git 
$ cd msa-gitlab-slide
$ npm install
$ npm start
```

브라우져를 통해 **http://localhost:9090** 에서 확인 할 수 있습니다.

## 데모 내용
[MSA 샘플 프로젝트](https://gitlab.com/GitLab-KR/workshop/201909-devops-kr/msa-sample) 의 README 를 보시면 진행된 데모의 내용을 간략히 정리 해두었습니다.
